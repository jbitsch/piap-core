package de.winteger.piap.mastergui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.winteger.piap.guicontent.AContentItem;
import de.winteger.piap.guicontent.CManager;

/**
 * A fragment representing a single content screen. This fragment is
 * either contained in a {@link ListActivity} in two-pane mode (on
 * tablets) or a {@link DetailActivity} on handsets.
 */
public class DetailFragment extends SherlockFragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	private AContentItem mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public DetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			mItem = CManager.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
			getActivity().setTitle(mItem.title);

			if (CManager.MENU != null) {
				CManager.MENU.clear();
				MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
				mItem.onCreateOptionsMenu(CManager.MENU, inflater);

			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		container.removeAllViews();
		View rootView = mItem.onCreateView(inflater, container, savedInstanceState);

		return rootView;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		mItem.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mItem.onSaveInstanceState(outState);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//		return super.onOptionsItemSelected(item);
		return mItem.onOptionsItemSelected(item);
	}

	@Override
	public void onPause() {
		mItem.onPause();
		super.onPause();
	}

	@Override
	public void onResume() {
		mItem.onResume();
		super.onResume();
	}
}
