package de.winteger.piap.guicontent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.winteger.piap.R;
import de.winteger.piap.core.InputLog;
import de.winteger.piap.data.DataSource;
import de.winteger.piap.helper.TimeHelper;
import de.winteger.piap.listadapter.ArrayAdapterInputLogs;

/**
 * Activity to view, delete, export logs
 * 
 * @author sarah
 * 
 */
public class ContentInputLogs extends AContentItem {

	private Context ctx;

	protected static final String TAG = "Logger";
	private DataSource datasource; // DB access object
	private ArrayList<InputLog> dbEntryList; // used to store the logs
	private ArrayAdapterInputLogs dbArrayAdapter; // notifies the view
	private ListView dbListView; // displays the logs
	private OnItemClickListener defaultListOnItemOnClickHandler = new OnItemClickListener() {

		//@Override
		public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
			// no special function for clicks on the list items
			Log.i(TAG, "Clicked on pos(" + pos + ") id(" + id + ")");
		}
	};

	public ContentInputLogs(String id, String title, int iconID) {
		super(id, title, iconID);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ctx = CManager.CTX;
		View rootView = inflater.inflate(R.layout.activity_view_input_logs, container, false);

		dbListView = (ListView) rootView.findViewById(R.id.lvInputLogs);
		dbEntryList = new ArrayList<InputLog>();

		// get all logs from the DB
		datasource = new DataSource(ctx);
		datasource.openWrite();
		datasource.getAllLogEntries(dbEntryList);
		datasource.close();

		LayoutInflater myInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// set up the view
		dbArrayAdapter = new ArrayAdapterInputLogs(ctx, myInflater, R.id.lvInputLogs, dbEntryList);
		dbListView.setAdapter(dbArrayAdapter);
		dbListView.setOnItemClickListener(defaultListOnItemOnClickHandler);
		dbListView.setOnItemLongClickListener(defaultListOnItemLongClickHandler);

		return rootView;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.activity_view_logs, menu);
		return true;
	}

	private OnItemLongClickListener defaultListOnItemLongClickHandler = new OnItemLongClickListener() {

		//@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view, int pos, long id) {
			// Log can be deleted from the DB on LongClick
			Log.i(TAG, "LongClicked on pos(" + pos + ") id(" + id + ")");

			final InputLog temp = dbEntryList.get(pos);

			AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);

			alertDialog.setPositiveButton(ctx.getString(R.string.delete), new DialogInterface.OnClickListener() {
				//@Override
				public void onClick(DialogInterface dialog, int which) {
					// delete log, notify gui
					datasource.openWrite();
					datasource.deleteLog(temp);
					datasource.getAllLogEntries(dbEntryList);
					datasource.close();

					dbArrayAdapter.notifyDataSetChanged();
				}
			});

			alertDialog.setNegativeButton(ctx.getString(R.string.cancel), null);
			alertDialog.setMessage(temp.getInput());
			alertDialog.setTitle(ctx.getString(R.string.delete_the_entry));
			alertDialog.show();

			return false;
		}

	};

	/**
	 * Handles the menu clicks
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_delete_all_logs:
			// delete all logs from the DB, notify view
			new AlertDialog.Builder(ctx).setTitle(ctx.getString(R.string.delete_all_logs))
					.setNegativeButton(ctx.getString(R.string.cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// ignore, just dismiss
						}
					}).setPositiveButton(ctx.getString(R.string.ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							datasource.openWrite();
							datasource.deleteAllLogs();
							datasource.getAllLogEntries(dbEntryList);
							datasource.close();

							dbArrayAdapter.notifyDataSetChanged();
						}
					}).setMessage(ctx.getString(R.string.this_will_delete_all_logs)).show();
			break;
		case R.id.menu_export_logs:
			// export logs to external storage
			new AlertDialog.Builder(ctx).setTitle(ctx.getString(R.string.export_logs))
					.setPositiveButton(ctx.getString(R.string.export), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// TODO: maybe in a thread? export date instead of timestamp..
							try {
								// creates the file (directory) if it does not already exist...
								File parentDirectory = new File(getExternalStorageDir() + "/PsychologistInAPocket/", "/Logs/");
								if (!parentDirectory.exists()) {
									Log.i(TAG, "It seems like the parent directory does not exist...");
									if (!parentDirectory.mkdirs()) {
										Log.i(TAG, "And we cannot create it...");
										// we have to return, throw or something else
									}
								}
								String fileName = System.currentTimeMillis() + "logs.txt"; // filename to export to
								File logFile = new File(parentDirectory, fileName);
								// write logs to file
								BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
								for (InputLog log : dbEntryList) {
									buf.write(TimeHelper.getDateTime(ctx, log.getTimestamp()) + ";" + log.getAppName() + ";" + log.getInput() + "\n");
								}
								buf.flush();
								buf.close();
								shortToast(ctx.getString(R.string.exported_logs_to) + logFile.getPath());
							} catch (FileNotFoundException e) {
								Log.i(TAG, "Could not save, file was not found" + e.getMessage());
							} catch (IOException e) {
								Log.i(TAG, "io exception" + e.getMessage());
							}
						}
					}).setNegativeButton(ctx.getString(R.string.cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// ignore, just dismiss
						}
					}).setMessage(ctx.getString(R.string.dialog_info_export_logs)).show();
			break;
		case R.id.sort_logs_time_ascending:
			// Sort logs by time, ascending
			sortLogsByTimeAscending();
			break;
		case R.id.sort_logs_time_descending:
			// Sort logs by time, descending
			sortLogsByTimeDescending();
			break;
		case R.id.sort_logs_by_app:
			// Sort logs by app
			sortLogsByApp();
			break;
		}
		return true;//super.onOptionsItemSelected(item);
	}

	/**
	 * Return the directory of the external storage
	 * 
	 * @return the directory
	 */
	public File getExternalStorageDir() {
		File extStore = Environment.getExternalStorageDirectory();
		return extStore;
	}

	/**
	 * Displays a short toast with message msg
	 * 
	 * @param msg
	 */
	private void shortToast(String msg) {

		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(ctx, msg, duration);
		toast.show();
	}

	/**
	 * sorts logs ascending by time
	 */
	private void sortLogsByTime() {
		Collections.sort(dbEntryList, new Comparator<InputLog>() {
			public int compare(InputLog l0, InputLog l1) {
				// an integer < 0 if lhs is less than rhs
				// 0 if they are equal
				// and > 0 if lhs is greater than rhs.
				if (l0.getTimestamp() == l1.getTimestamp()) {
					return 0;
				} else if (l0.getTimestamp() < l1.getTimestamp()) {
					return -1;
				} else {
					return 1;
				}
			}
		});
	}

	/**
	 * sorts logs ascending by time, notifies the view
	 */
	private void sortLogsByTimeAscending() {
		sortLogsByTime();
		dbArrayAdapter.notifyDataSetChanged();
	}

	/**
	 * sorts logs descending by time, notifies the view
	 */
	private void sortLogsByTimeDescending() {
		sortLogsByTime();
		Collections.reverse(dbEntryList);
		dbArrayAdapter.notifyDataSetChanged();
	}

	/**
	 * sorts logs ascending by app name, notifies the view
	 */
	private void sortLogsByApp() {
		Collections.sort(dbEntryList, new Comparator<InputLog>() {
			public int compare(InputLog l0, InputLog l1) {
				// an integer < 0 if lhs is less than rhs
				// 0 if they are equal
				// and > 0 if lhs is greater than rhs.
				return l0.getAppName().compareTo(l1.getAppName());
			}
		});
		dbArrayAdapter.notifyDataSetChanged();
	}

}
