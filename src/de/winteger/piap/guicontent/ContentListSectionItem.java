package de.winteger.piap.guicontent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import de.winteger.piap.R;

public class ContentListSectionItem extends AContentItem{

	public ContentListSectionItem(String id, String title) {
		super(id, title);
	}

	@Override
	public View onCreateListItemView(LayoutInflater inflater, View convertView, ViewGroup parent) {
		View rootView = inflater.inflate(R.layout.md_content_item_section, parent, false);
		TextView tvLabel = (TextView) rootView.findViewById(R.id.tvLabel);
		tvLabel.setText(title);
		return rootView;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return null;
	}
	
}
