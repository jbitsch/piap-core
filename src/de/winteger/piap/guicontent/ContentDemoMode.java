package de.winteger.piap.guicontent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import de.winteger.piap.R;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.evaluation.EvaluationScheduleManager;

public class ContentDemoMode extends AContentItem {
	private Button btnDemoModeSendSms;
	private Button btnDemoModeDisableMode;

	public ContentDemoMode(String id, String title) {
		super(id, title);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_demomode, container, false);

		btnDemoModeSendSms = (Button) rootView.findViewById(R.id.btnDemoModeSendTestSMS);
		btnDemoModeSendSms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//TODO: send sms
				GlobalSettings.sendSMSNotification("This is a PiaP test SMS");
			}
		});

		if (!GlobalSettings.isDemo()) {
			btnDemoModeDisableMode.setEnabled(false);
		}

		btnDemoModeDisableMode = (Button) rootView.findViewById(R.id.btnDemoModeDisableMode);
		btnDemoModeDisableMode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				GlobalSettings.setDemo(false);
				btnDemoModeDisableMode.setEnabled(false);
				EvaluationScheduleManager.updateSchedule(CManager.CTX);
			}
		});

		return rootView;
	}

}
