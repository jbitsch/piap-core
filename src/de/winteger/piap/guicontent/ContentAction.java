package de.winteger.piap.guicontent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import de.winteger.piap.R;

public class ContentAction extends AContentItem {

	public ContentAction(String id, String title, int iconID) {
		super(id, title, iconID);
	}

	@Override
	public View onCreateListItemView(LayoutInflater inflater, View convertView, ViewGroup parent) {
		View rootView = inflater.inflate(R.layout.md_content_item_simple, parent, false);
		ImageView ivIcon = (ImageView) rootView.findViewById(R.id.ivListIcon);

		ivIcon.setImageResource(iconID);
		
		TextView tvLabel = (TextView) rootView.findViewById(R.id.tvLabel);
		tvLabel.setText(title);
		return rootView;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// is not called as ContentAction has now view, only receives clicks in the menu
		return null;
	}

}
