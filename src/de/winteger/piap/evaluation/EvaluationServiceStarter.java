package de.winteger.piap.evaluation;

import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.guicontent.CManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Is called by the AlarmManager, starts the evaluation service
 * 
 * @author sarah
 * 
 */
public class EvaluationServiceStarter extends BroadcastReceiver {

	//private static final String TAG = "LoggerServiceStarter";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (CManager.CTX == null){
			CManager.CTX = context;
		}		
		Intent service = new Intent(context, EvaluationService.class);
		service.putExtra(GlobalSettings.EVAL_MODE, GlobalSettings.EVAL_MODE_SCHEDULE);
		context.startService(service);
	}
}