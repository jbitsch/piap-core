package de.winteger.piap.evaluation;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Stores the keywords for a specific category
 * 
 * @author sarah
 * 
 */
public class KeywordArray implements Serializable {

	private static final long serialVersionUID = -7529938750094664058L;

	public int category; // stores the category number

	public ArrayList<String> keywords; // list of all keywords for this category

	@Override
	public String toString() {
		return "KeywordArray [category=" + category + ", keywords=" + keywords + "]";
	}

	public KeywordArray() {
		super();
		keywords = new ArrayList<String>();
	}

	public KeywordArray(int category) {
		super();
		this.category = category;
		keywords = new ArrayList<String>();
	}

	public void addKeyWord(String keyword) {
		keywords.add(keyword);
	}

}
