package de.winteger.piap.evaluation;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Keywords/Category Source Access Object
 * 
 * @author sarah
 * 
 */
public class KeywordsDataSource implements Serializable {

	private static final long serialVersionUID = -1076059694758227540L;

	private ArrayList<KeywordArray> keywordsArrays;
	public static KeywordArray A1;
	public static KeywordArray A2;
	public static KeywordArray A3;
	public static KeywordArray A4;
	public static KeywordArray A5;
	public static KeywordArray A6;
	public static KeywordArray A7;
	public static KeywordArray A8;
	public static KeywordArray A9;
	public static KeywordArray A10;
	public static KeywordArray A11;
	public static KeywordArray A12;
	public static KeywordArray A13;
	public static KeywordArray A14;
	public static KeywordArray A15;
	public static KeywordArray A16;
	public static KeywordArray A17;
	public static KeywordArray A18;
	public static KeywordArray A19;
	public static KeywordArray A20;
	public static KeywordArray A21;
	public static KeywordArray A22;
	public static KeywordArray A23;
	public static KeywordArray A24;

	public static enum Category {
		DEPRESSED_MOOD(1), LOSS_OF_INTEREST(2), APPETITE_CHANGE(3), SLEEP_PROBLEMS(4), PSYCHOMOTOR_AGITATION(5), PSYCHOMOTOR_RETARDAION(6), LOSS_OF_ENGERGY(7), FEELINGS_OF_GUILT(
				8), INDECISIVNESS(9), SUICIDE_THOUGHTS(10), NEGATIVE_SELF_VIEW(11), NEGATIVE_VIEW_OF_EXPERIENCES(12), NEGATIVE_VIEW_OF_THE_FUTURE(13), IRRATIONAL_BELIEFS(
				14), EMOTIONAL_DEPRIVATION(15), ABANDONMENT(16), MISTRUST(17), SOCIAL_ISOLATION(18), SHAME(19), INFERIORITY(20), FAILURE(21), DEPENDENCE(22), VULNERABILITY(
				23), LACK_OF_SELF_DISCIPLINE(24);

		private int value;

		private Category(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}
	
	@Override
	public String toString() {
		return "KeywordsDataSource [keywordsArrays=" + keywordsArrays + "]";
	}

	/**
	 * only for testing
	 */
	@SuppressWarnings("unused")
	private void genrateArrays() {
		A1 = new KeywordArray(Category.DEPRESSED_MOOD.getValue());
		A1.addKeyWord("sad");
		A1.addKeyWord("lonely");

		A2 = new KeywordArray(Category.LOSS_OF_INTEREST.getValue());
		A1.addKeyWord("lost interest");
		A1.addKeyWord("not enthusiastic");
	}

	/**
	 * Adds all keyword arrays to an array list Returns the list
	 * 
	 * @return the keywords arrays
	 */
	public ArrayList<KeywordArray> addKeywordArraysToList() {
		keywordsArrays = new ArrayList<KeywordArray>();
		keywordsArrays.add(A1);
		keywordsArrays.add(A2);
		keywordsArrays.add(A3);
		keywordsArrays.add(A4);
		keywordsArrays.add(A5);
		keywordsArrays.add(A6);
		keywordsArrays.add(A7);
		keywordsArrays.add(A8);
		keywordsArrays.add(A9);
		keywordsArrays.add(A10);
		keywordsArrays.add(A11);
		keywordsArrays.add(A12);
		keywordsArrays.add(A13);
		keywordsArrays.add(A14);
		keywordsArrays.add(A15);
		keywordsArrays.add(A16);
		keywordsArrays.add(A17);
		keywordsArrays.add(A18);
		keywordsArrays.add(A19);
		keywordsArrays.add(A20);
		keywordsArrays.add(A21);
		keywordsArrays.add(A22);
		keywordsArrays.add(A23);
		keywordsArrays.add(A24);
		return keywordsArrays;
	}

	public ArrayList<KeywordArray> getKeywordsArrays() {
		return keywordsArrays;
	}

	public void setKeywordsArrays(ArrayList<KeywordArray> keywordsArrays) {
		this.keywordsArrays = keywordsArrays;
	}
}
