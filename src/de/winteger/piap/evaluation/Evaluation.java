package de.winteger.piap.evaluation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import android.content.Context;
import de.winteger.piap.R;
import de.winteger.piap.core.InputLog;
import de.winteger.piap.core.InputLogComperator;
import de.winteger.piap.helper.TimeHelper;

/**
 * Class that contains evaluation results and stats
 * 
 * @author sarah
 * 
 */
public class Evaluation implements Serializable {

	private static final long serialVersionUID = -6621217469594066276L;
	
	private int evalResult = EVAL_NO_RESULT;
	public static final int EVAL_NO_RESULT = -1;
	public static final int EVAL_NOT_DEPRESSED = 0;
	public static final int EVAL_MAY_BE_DEPRESSED = 1;
	
	private ArrayList<InputLog> relevantLogs; // logs that contain keywords
	private String info; // human-readable result of the evaluation
	private long timespanBegin; // date of the oldest considered log
	private long timespanEnd; // date of the newest considered log
	private int numberOfKeywords = 0; // number of found logs with keywords
	private int numberOfCategories = 0; // number of different found categories
	private boolean[] foundCategories = new boolean[25]; // foundCategories[x] = true if keyword from category x occurred

	
	
	public Evaluation(Context context) {
		info = context.getString(R.string.no_information_available);
		relevantLogs = new ArrayList<InputLog>();
	}

	public Evaluation(Context context, ArrayList<InputLog> relevantLogs) {
		info = context.getString(R.string.no_information_available);
		this.relevantLogs = relevantLogs;
	}

	public Evaluation(Context context, ArrayList<InputLog> relevantLogs, String info) {
		info = context.getString(R.string.no_information_available);
		this.relevantLogs = relevantLogs;
		this.info = info;
	}

	public void setCategoryStatus(int category) {
		if (category > 0 && category < 25) {
			foundCategories[category] = true;
		}
	}

	public boolean getCategoryStatus(int category) {
		if (category > 0 && category < 25) {
			return foundCategories[category];
		}
		return false;
	}

	@Override
	public String toString() {
		return "Evaluation [info=" + info + ", timespanBegin=" + timespanBegin + ", timespanEnd=" + timespanEnd + ", numberOfKeywords=" + numberOfKeywords
				+ ", numberOfCategories=" + numberOfCategories + ", foundCategories=" + Arrays.toString(foundCategories) + "]";
	}

	public void incrementNumberOfCategories() {
		numberOfCategories++;
	}

	public int getNumberOfkeywords() {
		return numberOfKeywords;
	}

	public void setNumberOfKeywords(int numberOfKeywords) {
		this.numberOfKeywords = numberOfKeywords;
	}

	public void computeNumberOfKeywords() {
		int res = 0;
		for (InputLog log : relevantLogs) {
			res += log.getNumberKeywords();
		}
		setNumberOfKeywords(res);
	}

	public int getNumberOfCategories() {
		return numberOfCategories;
	}

	public void setNumberOfCategories(int numberOfCategoris) {
		this.numberOfCategories = numberOfCategoris;
	}

	public long getTimespanBegin() {
		return timespanBegin;
	}

	public void setTimespanBegin(long timespanBegin) {
		this.timespanBegin = timespanBegin;
	}

	public long getTimespanEnd() {
		return timespanEnd;
	}

	public void setTimespanEnd(long timespanEnd) {
		this.timespanEnd = timespanEnd;
	}

	public ArrayList<InputLog> getRelevantLogs() {
		return relevantLogs;
	}

	public void setRelevantLogs(ArrayList<InputLog> relevantLogs) {
		this.relevantLogs = relevantLogs;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getEvalResult() {
		return evalResult;
	}
	public void setEvalResult(int evalResult) {
		this.evalResult = evalResult;
	}
	
	/**
	 * Stores in map and keys the number of keywords in each category for each
	 * day
	 * 
	 * @param map
	 *            The map of Dates and category counts for that day
	 * @param keys
	 *            The array list of Dates
	 */
	public void computeDailyData(HashMap<Date, int[]> map, ArrayList<Date> keys) {
		// Data
		ArrayList<InputLog> logs = relevantLogs;
		Collections.sort(logs, new InputLogComperator());
		for (InputLog log : logs) {
			Date d = TimeHelper.getDate(log.getTimestamp());
			if (map.containsKey(d)) {
				int[] values = map.get(d);
				// update stats
				for (int i = 1; i < 25; i++) {
					values[0] += log.getCategoryCounter(i); // incr 'total' counter
					values[i] += log.getCategoryCounter(i); // incr category counter
				}
				// update it
				map.put(d, values);
			} else {
				int[] values = new int[25];
				// update stats
				for (int i = 1; i < 25; i++) {
					values[0] += log.getCategoryCounter(i);
					values[i] += log.getCategoryCounter(i);
				}
				// add it
				map.put(d, values);
				keys.add(d);
			}
		}
	}
}
