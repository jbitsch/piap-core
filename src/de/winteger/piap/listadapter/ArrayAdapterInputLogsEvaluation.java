package de.winteger.piap.listadapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.winteger.piap.R;
import de.winteger.piap.core.EvalAccess;
import de.winteger.piap.core.InputLog;
import de.winteger.piap.helper.TimeHelper;

/**
 * This class defines how InputLogs are displayed in a ListView and updates the
 * ListView
 * 
 * @author sarah
 * 
 */
public class ArrayAdapterInputLogsEvaluation extends ArrayAdapter<InputLog> {

	private final String TAG = "Logger";
	private Context context; // Context that contains the ListView
	private LayoutInflater inflater; // Obtains the layout
	private ArrayList<InputLog> entryList; // List of InputLogs that is displayed in a ListView
	private int[] colorArray; // colors for the legend of categories

	/**
	 * Creates new ArrayAdapterInputLogs with the given parameters
	 * 
	 * @param context
	 * @param inflater
	 * @param textViewResourceId
	 * @param objects
	 */
	public ArrayAdapterInputLogsEvaluation(Context context, LayoutInflater inflater, int textViewResourceId, List<InputLog> objects) {
		super(context, textViewResourceId, objects);
		this.inflater = inflater;
		this.context = context;
		this.entryList = (ArrayList<InputLog>) objects;
		colorArray = EvalAccess.getCategoryColors(context);
	}

	/**
	 * sets the entryList to the list list
	 * 
	 * @param list
	 *            the list
	 */
	public void updateEntryList(ArrayList<InputLog> list) {
		entryList = list;
		Log.i(TAG, "List size: " + list.size());
	}

	/**
	 * defines how an item of the list view looks like
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// get the layout for a list item
		View entry = inflater.inflate(R.layout.lv_default_item, parent, false);

		TextView tvTime = (TextView) entry.findViewById(R.id.tvTime);
		TextView tvName = (TextView) entry.findViewById(R.id.tvName);
		TextView tvInput = (TextView) entry.findViewById(R.id.tvInput);
		TextView tvNumberOfKeywords = (TextView) entry.findViewById(R.id.tvNumberOfKeywords);
		tvNumberOfKeywords.setBackgroundColor(Color.argb(50, 255, 0, 0));
		LinearLayout llCategories = (LinearLayout) entry.findViewById(R.id.llHorizontalCategories);

		// get the input log
		InputLog item = entryList.get(position);

		// get data from the input log
		String time = item.getTimestamp() != 0 ? TimeHelper.getDateTime(context, item.getTimestamp()) : getContext().getString(R.string._no_time);
		String name = item.getAppName() != null ? item.getAppName() : getContext().getString(R.string._empty_name);
		String input = item.getInput() != null ? item.getInput() : getContext().getString(R.string._empty_input);
		String keywords = "" + item.getNumberKeywords();

		// fill the layout with the data
		tvTime.setText(time);
		tvName.setText(name);
		tvInput.setText(input);
		tvNumberOfKeywords.setText(keywords);

		// set category markers
		for (int i = 1; i < 25; i++) {
			if (item.getCategoryStatus(i)) {
				TextView t = new TextView(context);
				t.setMinEms(1);
				t.setGravity(Gravity.CENTER);
				t.setBackgroundColor(colorArray[i-1]);
				t.setTextColor(Color.WHITE);
				t.setText("" + i);
				llCategories.addView(t);
				// dummy TextView for space
				TextView s = new TextView(context);
				s.setMinimumWidth(2);
				llCategories.addView(s);
			}
		}

		return entry;
	}
}