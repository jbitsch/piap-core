package de.winteger.piap.listadapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import de.winteger.piap.R;
import de.winteger.piap.core.Plugin;
import de.winteger.piap.helper.DeviceInfo;

public class ArrayAdapterPlugins extends ArrayAdapter<Plugin> {
	private Context context;
	private LayoutInflater inflater;
	private ArrayList<Plugin> entryList;

	public ArrayAdapterPlugins(Context context, LayoutInflater inflater, int textViewResourceId, List<Plugin> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.inflater = inflater;
		this.entryList = (ArrayList<Plugin>) objects;
	}

	/**
	 * sets the entryList to the list list
	 * 
	 * @param list
	 *            the list
	 */
	public void updateEntryList(ArrayList<Plugin> list) {
		entryList = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// get the layout for a list item
		View entry = inflater.inflate(R.layout.lv_plugin_item, parent, false);

		ImageView ivPluginIcon = (ImageView) entry.findViewById(R.id.ivPluginIcon);
		TextView tvPluginName = (TextView) entry.findViewById(R.id.tvPluginName);
		TextView tvPluginState = (TextView) entry.findViewById(R.id.tvPluginState);

		final Plugin p = entryList.get(position);
		if (p != null) {

			ivPluginIcon.setImageResource(p.getIconID());
			tvPluginName.setText(p.getName());
			if (DeviceInfo.isInstalled(context, p.getPackageName())) {
				tvPluginState.setText(R.string.installed);
			} else {
				tvPluginState.setText(R.string.click_to_install);
				View.OnClickListener onClickInstall = new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						DeviceInfo.installPackage(context, p.getPackageName());
					}
				};
				tvPluginName.setOnClickListener(onClickInstall);
				tvPluginState.setOnClickListener(onClickInstall);
				ivPluginIcon.setOnClickListener(onClickInstall);
			}
		}

		return entry;
	}

}
