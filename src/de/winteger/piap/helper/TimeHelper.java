package de.winteger.piap.helper;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.content.Context;
import android.text.format.DateFormat;

/**
 * This class provides convenience methods for converting timestamps to dates
 * and vice versa
 * 
 * @author sarah
 * 
 */
public class TimeHelper {

	// Time intervals in milliseconds
	public static final long INTERVAL_HOUR = 3600000;
	public static final long INTERVAL_DAY = 86400000;
	public static final long INTERVAL_WEEK = 604800000;

	/**
	 * Returns the time of the calendar
	 * 
	 * @param c
	 *            the calendar
	 * @return the time
	 */
	public static long getTimestamp(Calendar c) {
		long time = c.getTimeInMillis();
		return time;
	}

	/**
	 * returns the date two weeks ago at midnight as timestamp
	 * 
	 * @return the timestamp
	 */
	public static long getTimestampTwoWeeksAgo() {
		Calendar rightNow = new GregorianCalendar();
		rightNow.set(Calendar.HOUR_OF_DAY, 24);
		rightNow.set(Calendar.MINUTE, 0);
		long res = rightNow.getTimeInMillis();
		res = res - 2 * TimeHelper.INTERVAL_WEEK;
		return res;
	}

	/**
	 * converts a time given in milliseconds to a human-readable date string
	 * 
	 * @param time
	 *            The Time
	 * @return The Timestring
	 */
	public static String getTimeString(long time) {
		long timeInSec = time / 1000;

		//int ss = ((int) timeInSec) % 60;
		int mm = ((int) timeInSec / 60) % 60;
		int hh = ((int) timeInSec) / 60 / 60;
		int d = hh / 24;
		hh = hh - d * 24;

		String ret;
		if (d == 0) {
			ret = String.format(Locale.US, "%dh %02dmin", hh, mm);
		} else {
			ret = String.format(Locale.US, "%dd %dh %02dmin", d, hh, mm);
		}

		return ret;
	}

	/**
	 * Returns the date and time for a given timestamp according to the current
	 * locale
	 * 
	 * @param context
	 * @param timestamp
	 * @return
	 */
	public static String getDateTime(Context context, long timestamp) {
		Date date = new Date(timestamp);
		String res = DateFormat.getDateFormat(context).format(date);
		res += " " + DateFormat.getTimeFormat(context).format(date);
		return res;
	}

	/**
	 * Returns the date for a given timestamp
	 * 
	 * @param timestamp
	 * @param type
	 * @return
	 */
	public static String getDate(long timestamp, int type) {
		String ds = null;

		switch (type) {

		default:
			Calendar c = new GregorianCalendar();
			c.setTimeInMillis(timestamp);

			ds = c.get(Calendar.DAY_OF_MONTH) + "." + (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
		}

		return ds;
	}

	/**
	 * Returns a Date initialized to the given year, month and day at midnight
	 * 
	 * @param timestamp
	 * @return
	 */
	public static Date getDate(long timestamp) {
		Date d;
		Calendar c = new GregorianCalendar();
		c.setTimeInMillis(timestamp);
		c.set(Calendar.HOUR_OF_DAY, 24);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		d = (new Date(c.getTimeInMillis() - INTERVAL_DAY));
		return d;
	}
}
